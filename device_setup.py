# For executing external programs and files.
import subprocess

# For exiting the program.
import sys

# Used for log messages.
import time

# Install the AWS IoT Device SDK for Python.
def install_iot_device():

    # Install zip and unzip (required to extract the AWS IoT Device SDK for Python).
    subprocess.call("apt-get install -y zip unzip", shell=True)

    # Unzip the AWS IoT Device SDK for Python and change the permissions.
    subprocess.call("unzip -n ./aws-iot-device-sdk-python-latest -d ~/SDK", shell=True)
    subprocess.call("chmod go+rwx ~/SDK", shell=True)

    # Install the AWS IoT Device SDK for Python.
    subprocess.call("cd ~/SDK && python setup.py install", shell=True)

try:
    f = open('device_setup_log.txt', 'w')
    subprocess.call("sudo chmod go+rwx ./device_setup_log.txt", shell=True)
    print( "\n%s The log file device_setup_log.txt was successfully created.\n" % time.asctime(time.localtime(time.time())) )

except:
    print( "\n%s Could not create the log file device_setup_log.txt.\n" % time.asctime(time.localtime(time.time())) )
    sys.exit()

def main():

    print("\n%s Started the setup for the Greengrass Device.\n" % time.asctime(time.localtime(time.time())))
    f.write("\n%s Started the setup for the Greengrass Device.\n" % time.asctime(time.localtime(time.time())))
    subprocess.call("apt-get update", shell=True)
    install_iot_device()
    print("\n%s Finished the setup for the Greengrass Device.\n" % time.asctime(time.localtime(time.time())))
    f.write("\n%s Finished the setup for the Greengrass Device.\n" % time.asctime(time.localtime(time.time())))
    f.close()

if __name__ == '__main__':
    main()
