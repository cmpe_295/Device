CMPE 295 Device

Getting Started

This program installs the remaining software required for the fog node/client to run as a Greengrass
device.

Prerequisites

The client must have Git installed to clone the repo.

Installing

This repo is cloned when the install dependencies script is executed
(ecep_client_cpu/ecep_client/install_dependencies.sh).

The Python file device_setup.py is executed by
ecep_client_cpu/ecep_client/install_dependencies.sh.

Versioning

v1.0

Authors

Sona Bhasin
Kanti Bhat
Johnny Nigh
Monica Parmanand
